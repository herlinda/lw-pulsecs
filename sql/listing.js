"use strict";

let sqlStatements =
    {
        "ALL_BY_AFFILIATE" :
`
SELECT
  a.affiliate_id,
  af.name                                           AS affiliate_name,
  af.mls_id                                         AS affiliate_mls_id,
  a.tx_id,
  a.property_type_id,
  c.name                                            AS property_type_name,
  a.tx_type,
  a.tx_side,
  a.marketing_plan_id,
  a.finance_id,
  a.sell_office_id,
  d.name                                            AS sell_office_name,
  a.sell_team_id,
  f.name                                            AS sell_team_name,
  a.buy_office_id,
  e.name                                            AS buy_office_name,
  a.buy_team_id,
  g.name                                            AS buy_team_name,
  a.sell_business_model_id,
  i.model_name                                      AS sell_business_model_name,
  a.buy_business_model_id,
  j.model_name                                      AS buy_business_model_name,
  a.created_on,
  a.created_by,
  a.modified_on,
  a.modified_by,
  a.withdrawn_on,
  a.reactivated_on,
  a.status,
  a.withdrawn,
  a.prop_street_no,
  a.prop_street_name,
  a.prop_unit,
  a.prop_city,
  a.prop_state,
  a.prop_country,
  k.code                                            AS prop_country_code,
  a.prop_postal_code,
  a.listing_price,
  a.list_date,
  a.orig_expiration_date,
  a.expiration_date,
  a.crest_id,
  a.pending_crest_id,
  a.mls_id,
  a.internal_id,
  a.lockbox_id,
  a.crest_guid,
  a.document_integrator_id,
  a.key_info,
  a.show_lockbox,
  a.show_appointment,
  a.show_access,
  a.show_notice,
  a.show_details,
  a.show_confidential,
  a.show_is_vacant,
  a.show_is_active,
  a.realtor_owned,
  a.headline,
  a.short_copy,
  a.long_copy,
  a.brochure_copy,
  a.vanity_url,
  a.photo_location,
  a.qr_code_location,
  a.agent_display_name,
  a.signage,
  a.signage_info,
  a.is_archived,
  txd.sell_side_finance_integrator_id,
  txd.buy_side_finance_integrator_id,
  txd.sell_side_relo,
  txd.sell_side_relo_company,
  txd.sell_side_relo_address,
  txd.sell_side_relo_phone,
  txd.buy_side_relo,
  txd.buy_side_relo_company,
  txd.buy_side_relo_address,
  txd.buy_side_relo_phone,
  txd.brochure_copy_long,
  txd.transaction_integrator_id,
  txd.transaction_integrator_version,
  txd.transaction_integrator_number,
  h.municipality_id,
  h.municipality_name,
  pulse.tx_clients_str(a.affiliate_id, a.tx_id, 10) AS seller_names,
  pulse.tx_clients_str(a.affiliate_id, a.tx_id, 20) AS buyer_names,
  pulse.tx_agents_str(a.affiliate_id, a.tx_id, 10)  AS selling_agent_names,
  pulse.tx_agents_str(a.affiliate_id, a.tx_id, 20)  AS buying_agent_names
FROM pulse.tx AS a
  LEFT OUTER JOIN pulse.tx_detail AS txd ON txd.affiliate_id = a.affiliate_id AND txd.tx_id = a.tx_id
  INNER JOIN pulse.affiliate AS af ON af.affiliate_id = a.affiliate_id
  INNER JOIN pulse.property_type AS c ON c.affiliate_id = a.affiliate_id AND c.type_id = a.property_type_id
  LEFT OUTER JOIN pulse.office AS d ON d.affiliate_id = a.affiliate_id AND d.office_id = a.sell_office_id
  LEFT OUTER JOIN pulse.office AS e ON e.affiliate_id = a.affiliate_id AND e.office_id = a.buy_office_id
  LEFT OUTER JOIN pulse.team AS f ON f.affiliate_id = a.affiliate_id AND f.team_id = a.sell_team_id
  LEFT OUTER JOIN pulse.team AS g ON g.affiliate_id = a.affiliate_id AND g.team_id = a.buy_team_id
  LEFT OUTER JOIN pulse.municipality AS h
    ON h.affiliate_id = a.affiliate_id AND h.municipality_id = a.prop_municipality_id
  LEFT OUTER JOIN pulse.business_model AS i ON i.affiliate_id = a.affiliate_id AND i.model_id = a.sell_business_model_id
  LEFT OUTER JOIN pulse.business_model AS j ON j.affiliate_id = a.affiliate_id AND j.model_id = a.buy_business_model_id
  LEFT OUTER JOIN pulse.country AS k ON k.name = a.prop_country
WHERE a.affiliate_id = 140 
`
};


module.exports = sqlStatements;
