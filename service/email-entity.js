"use strict";

let sql = require("../sql/entity-email");
let sqlFormat = require('sql-format-utils');
let S = require('string');
let ibmdb = require('ibm_db');
let strCon = require("../util/db-util").parseURL();
let _logger;


let EmailEntity = function (logger) {
    _logger = logger;

};

let _ = EmailEntity.prototype;


_.findEntity = (email, callback) => {
    let sqlEmail = sqlFormat.toSQLList(email.toLowerCase());
    sqlEmail = S(sql.FIND_USER).replaceAll('?', sqlEmail).s;

    ibmdb.open(strCon, function (err, conn) {
        if (err) return callback(err);
        conn.query(sqlEmail, function (err, data) {
            if (err) return callback(err);
            conn.close(function () {
                _logger.info("Done");
            });
            callback(null, data)
        });
    });
};

module.exports = EmailEntity;
