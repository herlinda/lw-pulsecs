var dbutil = require("./../config_files/db");

var  parseUrl = function() {
    return "DATABASE=" + dbutil.DATABASE + ";" +
           "HOSTNAME=" + dbutil.HOSTNAME + ";" +
           "UID="      + dbutil.UID      + ";" +
           "PWD="      + dbutil.PWD      + ";" +
           "PORT="     + dbutil.PORT     + ";" +
           "PROTOCOL=" + dbutil.PROTOCOL ;
};

module.exports = {
    parseURL : parseUrl
};

