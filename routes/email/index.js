let router = require('express').Router();
const fileUpload = require("express-fileupload");

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Email' });
});

router.get("/find/:email", require("./find-entity"));


router.get('/clean', function(req, res, next) {
    res.render('nulloutemail', { title: 'Clean Out' });
});

router.use('/bleach', fileUpload(), require("./bleach"));

module.exports = router;
