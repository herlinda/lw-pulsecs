"use strict";
let logger = require("./../../util/log-util").generalLogger;
let EmailEntity = require("./../../service/email-entity");
let RouteUtils = require("../../util/route-utils");
let path = require("path");
let dateFormat = require("date-format");
let uuid = require('node-uuid');
let S = require("string");
let sqlFormat = require('sql-format-utils');
let ibmdb = require('ibm_db');
let strCon = require("../../util/db-util").parseURL();
let sql = require("../../sql/entity-email");




function cleanArray(actual) {
    let newArray = new Array();
    for (let i = 0 ; i < actual.length; i++) {
        let element = S(actual[i]).trim().s;
        if (element) {
            newArray.push(element);
        }
    }
    return newArray;
}




function createSQL(actual,fileName) {
    let result =
        {
           "updSQL" : null ,
           "insSQL" : null
        };
    let updSQL = `BEGIN ATOMIC \n`;
    let insSQL = `BEGIN ATOMIC \n`;
    let sql = "INSERT INTO PULSE.EMAIL_NULL_LOG (BATCH_ID,TABLE_NAME,AFFILIATE_ID,ENTITY_ID,EMAIL,UPD_SQL,CREATED_ON) VALUES ("
              + escapeSingleQuotes(fileName) + ",";

    for (let i = 0 ; i < actual.length; i++) {

        let  rsql = sql + escapeSingleQuotes(actual[i].TABLE)
            + ","
            + actual[i].AFFILIATE_ID
            + ","
            + actual[i].ENTITY_ID
            +  ","
            +escapeSingleQuotes(actual[i].EMAIL)
            +  ","
            + escapeSingleQuotes(actual[i].UPD_SQL)
            + ",CURRENT_TIMESTAMP);" ;
        insSQL += rsql + "\n";
        updSQL += actual[i].UPD_SQL + ";\n";
    }
     insSQL += " END";
     updSQL += " END";
     result.insSQL = insSQL;
     result.updSQL = updSQL;
     return result;
}

function escapeSingleQuotes( str) {
    if (str === null) {
        return null;
    }
    return "\'" + str + "\'";
}


module.exports= function (req, res, next) {
    let dataFile =  req.files.datafile;
    if (dataFile === undefined ) {
        return res.status(404).send("No files were uploaded");
    }

    let fileName = dateFormat.asString("yyyy-MM-dd-", new Date()) + uuid.v1() + ".txt";

    fileName = path.join(__dirname , "../../upload",fileName);

    dataFile.mv(fileName, function(err) {
        if (err) {
            return res.status(500).send(err);
        }

        let badEmails = S(dataFile.data.toString())
                            .replaceAll("\r","")
                            .split("\n");
        badEmails = cleanArray(badEmails);
        if (badEmails === null || badEmails.length === 0) {
            return res.status(404).send("Uploaded file is empty");
        }



        let sqlIN = sqlFormat.toSQLList(badEmails).toLowerCase();
        let sqlEmail = S(sql.FIND_USER).replaceAll('?', sqlIN).s;

        ibmdb.open(strCon, function (err, conn) {
            if (err) return res.status(500).send(err);
            conn.query(sqlEmail, function (err, data) {
                if (err) return res.status(500).send(err);
                if (data === undefined || data === null || data.length === 0) {
                    return res.status(404).send("No emails found in the database");
                }

                let sqlScr = createSQL(data,fileName);


                logger.info("Execute Insert" );
                logger.info(sqlScr.insSQL);

                let stmtINS = conn.prepareSync(sqlScr.insSQL);
                let resultINS = stmtINS.executeSync();
                let insdata = resultINS.fetchAllSync({fetchMode:3}); // Fetch data in Array mode.
                logger.info(insdata);
                resultINS.closeSync();
                stmtINS.closeSync();

                logger.info("Execute Update" );
                logger.info(sqlScr.updSQL);

                let stmtUPD = conn.prepareSync(sqlScr.updSQL);
                let resultUPD = stmtUPD.executeSync();
                let upddata = resultUPD.fetchAllSync({fetchMode:3}); // Fetch data in Array mode.
                logger.info(upddata);
                resultUPD.closeSync();
                stmtUPD.closeSync();

                res.status(200).json(data);
                conn.close(function () {

                });
            });
        });

    });


};
