"use strict";
let json2csv = require("json2csv");
let logger = require("./../../util/log-util").generalLogger;
let EmailEntity = require("./../../service/email-entity");
let RouteUtils = require("../../util/route-utils");
let csv = require("express-csv");

module.exports= function (req, res, next) {
    let emailEntity = new EmailEntity(logger);
    let response ;
    emailEntity.findEntity(req.params.email ,(err, data)=>{
        if (err) {
            return RouteUtils.sendInternalServerError(res);
        } else if (data === null || data.length === 0) {
            return RouteUtils.sendClientError(
                res,
                404,
                "Email not found",
                "Unable to find an email address = " + req.params.email)
            ;
        }
        /*
        let result = null;
        try {
             result = json2csv({"data": data, "fields" :['AFFILIATE_NAME','TABLE','AFFILIATE_SHORT_NAME','AFFILIATE_ID','ENTITY_ID', 'NAME','EMAIL','ALT_EMAIL']} );
             logger.info(result);




        } catch (err) {
            logger.info(err.message);
            return RouteUtils.sendInternalServerError(res);
        }

       // res.status(200).csv(result);
       */

        res.status(200).json(data);


    }) ;
};
