var ibmdb = require('ibm_db');
var strCon = require("../util/db-util").parseURL();

ibmdb.open(strCon, function (err,conn) {
    if (err) return console.log(err);

    conn.query('select * from syspublic.dual', function (err, data) {
        if (err) console.log(err);
        else console.log(data);

        conn.close(function () {
            console.log('done');
        });
    });
});

