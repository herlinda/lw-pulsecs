let ibmdb = require('ibm_db');
let emailCheck = require("email-check");
let strCon = require("../util/db-util").parseURL();
let logger = require("../util/log-util" ).generalLogger;
let fs = require("fs");
let json2xls = require('json2xls');


let query =
`
SELECT a.AFFILIATE_ID , 
       a.SHORT_NAME  AFFILIATE_NAME , 
       c.CLIENT_ID, 
       c.NAME CLIENT_NAME, 
       c.CNT_EMAIL EMAIL ,
       c.CREATED_ON, 
       c.CREATED_BY 
FROM PULSE.AFFILIATE a 
INNER JOIN PULSE.CLIENT c ON ( a.AFFILIATE_ID = c.AFFILIATE_ID  ) 
WHERE  (c.CNT_EMAIL is not null) 
ORDER BY a.SHORT_NAME, c.NAME 
  
`;

//




ibmdb.open(strCon, function (err, conn) {
    console.log("Starts");
    if (err) return console.log(err);

    conn.query(query, function (err, data) {
        if (err) {
            console.log(err);

        }
        else {
            console.log("data retrieved");
        }
        conn.close(function () {
            console.log("Closed Connection");
            logger.info(data);
            //validateRetrievedEmails(data);
            console.log("Ends");
        });

    });
});



function validateRetrievedEmails(dataRetrieved) {
    console.log("Processing data");
    for (let i = 0; i < dataRetrieved.length; i++) {
        let myEmail = dataRetrieved[i].EMAIL;
        emailCheck(myEmail)
            .then(function (res) {
                if (res) {
                   console.log(i + " " + myEmail + " Valid");
                }
                else {
                    //console.log(i + " " + myEmail + " Invalid");
                    logger.info(dataRetrieved[i]);
                }

            })
            .catch(function (err) {
                if (err.message === 'refuse') {
                    console.log("The MX server is refusing requests from your IP address.");
                } else {
                    console.log(i + " " +  myEmail + " Error " + error.message);

                }

            });
    }

}
/*
function writeFiles(next) {
    let xls = json2xls(badGuys);
    let jsonData = JSON.stringify(badGuys);
    fs.writeFileSync('/opt/tmp/agents_bad_email.json', jsonData, '');
    fs.writeFileSync('/opt/tmp/agents_bad_email.xls', xls, 'binary');
    next(null, "writeFiles");
}

*/



