var ibmdb = require('ibm_db');
var validator = require("email-validator");
var strCon = require("../util/db-util").parseURL();
var fs = require("fs");
var json2xls = require('json2xls');


var query =
    `
SELECT a.AFFILIATE_ID , 
       a.SHORT_NAME  AFFILIATE_NAME , 
       c.CLIENT_ID, 
       c.NAME CLIENT_NAME, 
       c.CNT_EMAIL EMAIL , 
       c.CNT_ALT_EMAIL ALT_EMAIL,
       c.CREATED_ON, 
       c.CREATED_BY, 
       c.MODIFIED_BY 
FROM PULSE.AFFILIATE a 
INNER JOIN PULSE.CLIENT c ON ( a.AFFILIATE_ID = c.AFFILIATE_ID  ) 
ORDER BY a.SHORT_NAME, c.NAME 
`;

//FETCH FIRST 200 ROWS ONLY


var badGuys = [];
ibmdb.open(strCon, function (err, conn) {
    if (err) return console.log(err);

    conn.query(query, function (err, data) {
        if (err) console.log(err);
        else {
            console.log("data retrieved");

        }
        conn.close(function () {
            var xls = json2xls(data);
            //var jsonData = JSON.stringify(badGuys);
            //fs.writeFileSync('/opt/tmp/all_agents_inv_email.json', jsonData, '');
            fs.writeFileSync('/opt/tmp/all_agents_inv_email.xls', xls, 'binary');
            console.log('done');
        });
    });
});


/*
function validateRetrievedEmails(data, callback) {
    console.log("Processing data");
    for (var i = 0; i < data.length; i++) {
        if (validator.validate(data[i].EMAIL)) {
            console.log(data[i].EMAIL + " Valid");
        }
        else {
            console.log(data[i].EMAIL + " Invalid");
            badGuys.push(data[i]);
        }
    }
}

*/


