var config = require("../config_files/logger-config");
var winston = require("winston");
var expressWinston = require("express-winston");

var myTransports = [];


for (var i = 0; i < config.transports.length; i++) {
    var myTransport = config.transports[i];
    if (myTransport.console) {
        myTransports.push(new (winston.transports.Console)(myTransport.options))
    }
    if (myTransport.file) {
        myTransports.push(new (winston.transports.File)(myTransport.options))
    }
}

var logger = new winston.Logger({transports: myTransports } );
var expressLogger = expressWinston.logger({ transports: myTransports } );

module.exports = {
   generalLogger :  logger ,
   webLogger     :  expressLogger
};


