'use strict';

var ClientError = function (code, message, details) {
    this.Code = code;
    this.Message = message == null ? null : message;
    this.Details = details == null ? null : details;
};

var Utils;

Utils = {
    sendClientError: function (res, statusCode, message, details) {
        var ce = new ClientError(statusCode, message, details);
        res.status(statusCode).json(ce);
    },
    sendInternalServerError: function (res) {
        var ce = new ClientError(
            500,
            "Unknown error occurred",
            "Please contact Lone Wolf Real Estate Technologies for support.");

        res.status(500).json(ce);
    }

};

module.exports = Utils;

